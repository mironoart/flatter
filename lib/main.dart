import 'package:flutter/material.dart';

import './quiz.dart';
import './result.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  final _questions = const [
    {
      'questionText': 'What is JavaScript?',
      'answers': [
        {'text': 'Programming language', 'score': 1},
        {'text': 'Script language', 'score': 0},
        {'text': 'Web language', 'score': 0},
      ]
    },
    {
      'questionText': 'What is variable?',
      'answers': [
        {'text': 'Place where you can store a number', 'score': 0},
        {'text': 'Special word or character to use several times', 'score': 0},
        {'text': 'So called containers for storing data values', 'score': 1},
      ]
    },
    {
      'questionText': 'What is boolean?',
      'answers': [
        {
          'text': 'Primitive data type which can be "true" or "false"',
          'score': 1
        },
        {'text': 'Variable which can be "true" or "false"', 'score': 0},
        {
          'text': 'Data type which can be used to only in if..else statements',
          'score': 0
        },
      ]
    },
  ];
  var _questionIndex = 0;
  var _totalScore = 0;

  void _resetQuiz() {
    setState(() {
      _questionIndex = 0;
      _totalScore = 0;
    });
  }

  void _answerQuestions(int score) {
    _totalScore += score;
    setState(() {
      if (_questionIndex < _questions.length)
        _questionIndex = _questionIndex + 1;
    });
  }

  @override
  Widget build(BuildContext ctx) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('My First App'),
        ),
        body: _questionIndex < _questions.length
            ? Quiz(
                answerQuestions: _answerQuestions,
                questionIndex: _questionIndex,
                questions: _questions,
              )
            : Result(_totalScore, _resetQuiz),
      ),
    );
  }
}
